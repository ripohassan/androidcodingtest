package com.lifeplus.test.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.lifeplus.test.MainActivity;
import com.lifeplus.test.R;
import com.lifeplus.test.Utils.AppPrefManager;

public class LoginActivity extends AppCompatActivity {

    private EditText userName,userPassword;
    private Context mContext;
    private AppPrefManager appPrefManager;
    private String name,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    initVariables();
    initView();
    initFunction();
    initListener();
}

    private void initVariables() {
        mContext = getApplicationContext();
        appPrefManager = new AppPrefManager(mContext);
    }

    private void initView() {
        setContentView(R.layout.activity_login);
        userName = findViewById(R.id.edt_user_name);
        userPassword = findViewById(R.id.edt_password);

    }

    private void initFunction() {

        name = appPrefManager.getUserDetails().get(AppPrefManager.KEY_USERNAME);
        password = appPrefManager.getUserDetails().get(AppPrefManager.KEY_PASSWORD);


    }

    private void initListener() {

        findViewById(R.id.tv_goRegistration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });

        findViewById(R.id.img_goLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (userName.equals("")) {
                    Toast.makeText(mContext, getString(R.string.Enter_Your_User_Name), Toast.LENGTH_SHORT).show();
                } else if (userPassword.length() < 4) {
                    Toast.makeText(mContext, getString(R.string.Your_User_Name_To_Short), Toast.LENGTH_SHORT).show();

                } else if (userPassword.equals("")) {
                    Toast.makeText(mContext, getString(R.string.Enter_Your_Password), Toast.LENGTH_SHORT).show();

                } else if (userPassword.length() < 6) {
                    Toast.makeText(mContext, getString(R.string.Enter_Password), Toast.LENGTH_SHORT).show();


                }else {
                    String uName = userName.getText().toString().trim();
                    String uPassword = userPassword.getText().toString().trim();

                    if (appPrefManager.isLoggedIn()){
                        if (name.contains(uName) || uPassword.contains(uPassword) ){
                            gotoDashBoard();
                            Toast.makeText(mContext,getString(R.string.login_success), Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(mContext, getString(R.string.login_fail), Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(mContext, getString(R.string.login_cred), Toast.LENGTH_SHORT).show();

                    }



                }
            }
        });


        findViewById(R.id.showPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(userPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.hide);
                //Show Password
                userPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.view);
                //Hide Password
                userPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }

        });
    }

    private void gotoDashBoard() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }
}

