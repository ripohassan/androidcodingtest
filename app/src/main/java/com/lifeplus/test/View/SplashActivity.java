package com.lifeplus.test.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;

import com.lifeplus.test.MainActivity;
import com.lifeplus.test.R;
import com.lifeplus.test.Utils.AppPrefManager;

public class SplashActivity extends AppCompatActivity {
    // Fields ========================
    private Context mContext;
    private final Handler mHandler = new Handler();
    private AppPrefManager appPrefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariables();
        initView();
       // initFunction();
        //initListener();
    }

    private void initVariables() {
        mContext = getApplicationContext();
        appPrefManager = new AppPrefManager(mContext);
        mHandler.postDelayed(mPendingLauncherRunnable, 3000);

    }

    private void initView() {
        setContentView(R.layout.activity_splash);
    }
    private final Runnable mPendingLauncherRunnable = new Runnable() {
        @Override
        public void run() {
            if (appPrefManager.isLoggedIn()){
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            }else {
                startActivity(new Intent(SplashActivity.this,LoginActivity.class));
            }
            finish();

        }
    };

}