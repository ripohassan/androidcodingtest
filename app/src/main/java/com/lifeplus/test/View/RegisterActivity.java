package com.lifeplus.test.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.lifeplus.test.R;
import com.lifeplus.test.Utils.AppPrefManager;

public class RegisterActivity extends AppCompatActivity {
    private Context mContext;
    private EditText name,userName,password,phone;
    private AppPrefManager appPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    initVariables();
    initView();
   // initFunction();
    initListener();
}

    private void initVariables() {
        mContext = getApplicationContext();
        appPrefManager = new AppPrefManager(mContext);
    }

    private void initView() {
        setContentView(R.layout.activity_register);
        name = findViewById(R.id.edt_name);
        userName = findViewById(R.id.edt_user_name);
        password = findViewById(R.id.edt_password);
        phone = findViewById(R.id.edt_phone);
    }

    private void initListener() {
        findViewById(R.id.tv_goRegistration).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoLogin();
            }
        });

        findViewById(R.id.tv_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.equals("")){
                    Toast.makeText(mContext, getString(R.string.enter_your_name), Toast.LENGTH_SHORT).show();
                }else if (name.length() < 4 ){
                    Toast.makeText(mContext, getString(R.string.Your_Name_To_Short), Toast.LENGTH_SHORT).show();
                }
                else if (userName.equals("")){
                    Toast.makeText(mContext, getString(R.string.Enter_Your_User_Name), Toast.LENGTH_SHORT).show();
                }else if (userName.length() < 4 ){
                    Toast.makeText(mContext, getString(R.string.Your_User_Name_To_Short), Toast.LENGTH_SHORT).show();

                }else if (password.equals("")){
                    Toast.makeText(mContext,getString( R.string.Enter_Your_Password), Toast.LENGTH_SHORT).show();

                }else if (password.length() < 6){
                    Toast.makeText(mContext,getString( R.string.Enter_Password), Toast.LENGTH_SHORT).show();

                }else if (phone.equals("")){
                    Toast.makeText(mContext,getString( R.string.Enter_Your_Phone_Number), Toast.LENGTH_SHORT).show();

                }else if (phone.length() < 11){
                    Toast.makeText(mContext, getString(R.string.Your_Phone_Number_To_Short), Toast.LENGTH_SHORT).show();

                }else{
                    String uName = name.getText().toString().trim();
                    String uSName = userName.getText().toString().trim();
                    String uPassword = password.getText().toString().trim();
                    String uPhone = phone.getText().toString().trim();
                    appPrefManager.storeRegisterData(uName,uSName,uPassword,uPhone);

                    if(appPrefManager.isLoggedIn()){
                        gotoLogin();
                        Toast.makeText(mContext, getString(R.string.account_create_successfully), Toast.LENGTH_SHORT).show();

                    }else {
                        Toast.makeText(mContext, getString(R.string.register_fail), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void gotoLogin() {
        startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
        finish();
    }
}