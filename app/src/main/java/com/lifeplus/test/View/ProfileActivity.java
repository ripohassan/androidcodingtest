package com.lifeplus.test.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.lifeplus.test.MainActivity;
import com.lifeplus.test.R;
import com.lifeplus.test.Utils.AppPrefManager;

public class ProfileActivity extends AppCompatActivity {

    private Context mContext;
    private AppPrefManager appPrefManager;
    private TextView name,userName,phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    initVariables();
    initView();
    initFunction();
    initListener();
}

    private void initVariables() {
        mContext = getApplicationContext();
        appPrefManager = new AppPrefManager(mContext);
    }

    private void initView() {
        setContentView(R.layout.activity_profile);
        name  = findViewById(R.id.tv_name);
        userName  = findViewById(R.id.tv_user_name);
        phone = findViewById(R.id.tv_phone);

    }

    private void initFunction() {

        // get and set data
        name.setText(appPrefManager.getUserDetails().get(AppPrefManager.KEY_NAME));
        userName.setText(appPrefManager.getUserDetails().get(AppPrefManager.KEY_USERNAME));
        phone.setText(appPrefManager.getUserDetails().get(AppPrefManager.KEY_PHONE));


    }

    private void initListener() {

        // back button press
        findViewById(R.id.imv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });
    }

}