package com.lifeplus.test.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lifeplus.test.Model.ShowsDetails;
import com.lifeplus.test.R;
import com.lifeplus.test.Utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ShowAdapter extends RecyclerView.Adapter<ShowAdapter.ShowListView> implements Filterable {
    LayoutInflater layoutInflater;
    Context context;
    List<ShowsDetails> detailsArrayList;
    List<ShowsDetails> filteredArrayList;

    ItemClickListener itemClickListener;

    public ShowAdapter(Context context, ArrayList<ShowsDetails> detailsArrayList) {
        this.context = context;
        this.detailsArrayList = detailsArrayList;
        this.filteredArrayList = detailsArrayList;
        layoutInflater=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ShowListView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=  layoutInflater.inflate(R.layout.item_show,parent,false);
        return new ShowListView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowListView holder, int position) {

        holder.name.setText(detailsArrayList.get(position).getName());
        holder.country.setText(detailsArrayList.get(position).getCountry().getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onClick(view,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredArrayList.size();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredArrayList = detailsArrayList;
                } else {
                    List<ShowsDetails> filterList = new ArrayList<>();
                    for (ShowsDetails row : detailsArrayList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filterList.add(row);
                        }
                    }

                    filteredArrayList = filterList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredArrayList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredArrayList = (ArrayList<ShowsDetails>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ShowListView extends RecyclerView.ViewHolder {
        TextView name, country,time;
        public ShowListView(@NonNull View itemView) {
            super(itemView);
            name =itemView.findViewById(R.id.tv_name);
            country =itemView.findViewById(R.id.tv_country);

        }
    }

    public void setClickListener(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }
}