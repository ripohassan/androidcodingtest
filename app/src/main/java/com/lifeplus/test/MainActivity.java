package com.lifeplus.test;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.lifeplus.test.Adapter.ShowAdapter;
import com.lifeplus.test.Model.Country;
import com.lifeplus.test.Model.ShowsDetails;
import com.lifeplus.test.Network.RetrofitClient;
import com.lifeplus.test.Utils.AppPrefManager;
import com.lifeplus.test.Utils.ItemClickListener;
import com.lifeplus.test.View.ProfileActivity;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements ItemClickListener {

    private Context mContext;
    private RecyclerView rvShowList;
    private ShowAdapter showAdapter;
    private ProgressBar progressBar;
    private ArrayList<ShowsDetails> showsDetails;
    private EditText editSearch;
    private TextView name,countryName,countryCode,timeZone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariables();
        initView();
        initFunction();
        initListener();
    }

    private void initVariables() {

        mContext = getApplicationContext();
        showsDetails = new ArrayList<>();

    }


    private void initView() {
        setContentView(R.layout.activity_main);
        rvShowList = findViewById(R.id.rvShowList);
        progressBar = findViewById(R.id.progressBar);
        editSearch = findViewById(R.id.edt_search);
    }

    private void initFunction() {
        loadShowDetails();
    }

    private void initListener() {

        findViewById(R.id.imv_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoProfile();
            }
        });

        findViewById(R.id.searchImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = editSearch.getText().toString().trim();
                showAdapter.getFilter().filter(text);

            }
        });

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                showAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void gotoProfile() {
        startActivity(new Intent(MainActivity.this, ProfileActivity.class));
    }

    // load data from Api
    private void loadShowDetails() {
        progressBar.setVisibility(View.VISIBLE);
        RetrofitClient.getClient().getPersonList().enqueue(new Callback<List<ShowsDetails>>() {
            @Override
            public void onResponse(Call<List<ShowsDetails>> call, Response<List<ShowsDetails>> response) {
                if (response.body() != null) {
                    showsDetails.clear();
                    showsDetails.addAll(response.body());
                    progressBar.setVisibility(View.GONE);
                    loadData();
                }
            }

            @Override
            public void onFailure(Call<List<ShowsDetails>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.d("response fail",""+t.getMessage());
            }
        });
    }


    // set data on recyclerview
    private void loadData() {
        rvShowList.setVisibility(View.VISIBLE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        rvShowList.setLayoutManager(layoutManager);
        showAdapter = new ShowAdapter(this, showsDetails);
        rvShowList.setAdapter(showAdapter);
        showAdapter.setClickListener(this);
    }

    // recyclerview item click
    @Override
    public void onClick(View view, int position) {
        showDetailsDialog(view,showsDetails.get(position).getName(),showsDetails.get(position).getCountry().getName(),
                showsDetails.get(position).getCountry().getCode(),showsDetails.get(position).getCountry().getTimezone());
    }


    // show details popup view
    private void showDetailsDialog(View view,String showName,String showCountryName,String showCountryCode,String showTimeZone) {
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.details_popup, null);
        name = popupView.findViewById(R.id.tv_name);
        countryName = popupView.findViewById(R.id.tv_c_name);
        countryCode = popupView.findViewById(R.id.tv_code);
        timeZone = popupView.findViewById(R.id.tv_time);

        name.setText(showName);
        countryName.setText(showCountryName);
        countryCode.setText(showCountryCode);
        timeZone.setText(showTimeZone);
        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.setAnimationStyle(R.style.WindowAnimations);

        // show the popup window
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }


}