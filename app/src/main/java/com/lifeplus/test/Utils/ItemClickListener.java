package com.lifeplus.test.Utils;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view,int position);
}
