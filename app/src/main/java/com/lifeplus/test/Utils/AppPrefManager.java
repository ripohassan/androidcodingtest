package com.lifeplus.test.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class AppPrefManager {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context mContext;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "PREF_OF_USER_INFO";
    public static final String KEY_NAME = "name";
    public static final String KEY_USERNAME = "userName";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_PHONE = "phone";


    private static final String USER_IS_LOGIN = "IsLoggedIn";
   

    public AppPrefManager(Context context) {
        this.mContext = context;
        sharedPreferences = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    // store user all information
    public void storeRegisterData(String name, String userName, String password, String phone) {
        editor.putBoolean(USER_IS_LOGIN, true);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_USERNAME, userName);
        editor.putString(KEY_PASSWORD, password);
        editor.putString(KEY_PHONE, phone);
        editor.commit();
    }


    // get user all information
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> userData = new HashMap<String, String>();
        userData.put(KEY_NAME, sharedPreferences.getString(KEY_NAME, null));
        userData.put(KEY_USERNAME, sharedPreferences.getString(KEY_USERNAME, null));
        userData.put(KEY_PASSWORD, sharedPreferences.getString(KEY_PASSWORD, null));
        userData.put(KEY_PHONE, sharedPreferences.getString(KEY_PHONE, null));
        return userData;
    }

    // Logout user
    public void logoutUser() {
        sharedPreferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(USER_IS_LOGIN, false).apply();
        sharedPreferences.edit().remove(KEY_NAME).apply();
        sharedPreferences.edit().remove(KEY_USERNAME).apply();
        sharedPreferences.edit().remove(KEY_PASSWORD).apply();
        sharedPreferences.edit().remove(KEY_PHONE).apply();
      
    }

    //Check is user Login or not
    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(USER_IS_LOGIN, false);
    }
    
}
