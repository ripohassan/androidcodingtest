package com.lifeplus.test.Model;

public class UserInfoModel {
 int userId;
 String name,userName,phone,password;

    public UserInfoModel(int userId, String name, String userName, String phone, String password) {
        this.userId = userId;
        this.name = name;
        this.userName = userName;
        this.phone = phone;
        this.password = password;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
