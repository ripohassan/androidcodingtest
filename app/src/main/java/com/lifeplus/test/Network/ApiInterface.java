package com.lifeplus.test.Network;


import com.lifeplus.test.Model.ShowsDetails;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface ApiInterface {
    @GET(HttpParams.END_POINT)
    Call<List<ShowsDetails>> getPersonList();
}
